using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerPosition
/// </summary>
public class ControllerPosition : ClassBase
{
    public ControllerPosition(DataClassesDatabaseDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public Table_Position[] Data()
    {
        return db.Table_Positions.ToArray();
    }

    public Table_Position Create(string name)
    {
        Table_Position position = new Table_Position
        {
            Name = name,
            CreatedBy = 1,
            UID = Guid.NewGuid(),
            CreatedAt = DateTime.Now,
        };
        db.Table_Positions.InsertOnSubmit(position);
        return position;
    }

    public Table_Position Cari(string UID)
    {
        return db.Table_Positions.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    public Table_Position Update(string UID, string name)
    {
        var position = Cari(UID);
        if (position != null)
        {
            position.Name = name;
            position.CreatedBy = 1;

            return position;
        }
        else
        {
            return null;
        }
    }

    public Table_Position Delete(string UID)
    {
        var brand = Cari(UID);
        if (brand != null)
        {
            db.Table_Positions.DeleteOnSubmit(brand);
            db.SubmitChanges();
            return brand;
        }
        else
        {
            return null;
        }
    }

    public void DropDownListPosition(DropDownList dropDownList)
    {
        dropDownList.DataSource = Data();
        dropDownList.DataValueField = "ID";
        dropDownList.DataTextField = "Name";
        dropDownList.DataBind();

        dropDownList.Items.Insert(0, new ListItem { Value = "0", Text = "-Pilih-" });

    }

    public ListItem[] DropDownList()
    {
        List<ListItem> position = new List<ListItem>();

        position.Add(new ListItem { Value = "0", Text = "-Pilih-" });

        position.AddRange(Data().Select(x => new ListItem
        {
            Value = x.ID.ToString(),
            Text = x.Name,
        }));

        return position.ToArray();
    }
}