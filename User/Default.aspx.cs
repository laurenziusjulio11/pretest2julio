﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();
        }
    }

    public void LoadData()
    {

        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            repeaterUser.DataSource = db.Table_Users.Select(x => new
            {
                x.ID,
                x.UID,
                x.Name,
                NameCompany = x.Table_Company.Name,
                NamePosition = x.Table_Position.Name,
                x.Address,
                x.Email,
                x.Telephone

            }).ToArray();
            repeaterUser.DataBind();
        }
    }


    protected void repeaterUser_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/User/Form.aspx?id=" + e.CommandArgument.ToString());
            }
            else if (e.CommandName == "Delete")
            {
                var user = db.Table_Users.FirstOrDefault(x => x.ID.ToString() == e.CommandArgument.ToString());

                db.Table_Users.DeleteOnSubmit(user);
                db.SubmitChanges();

                LoadData();
            }
        }
    }


}